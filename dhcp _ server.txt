System Bootstrap, Version 12.1(3r)T2, RELEASE SOFTWARE (fc1)
Copyright (c) 2000 by cisco Systems, Inc.
PT 1001 (PTSC2005) processor (revision 0x200) with 60416K/5120K bytes of memory

Readonly ROMMON initialized

Self decompressing the image :
########################################################################## [OK]

              Restricted Rights Legend

Use, duplication, or disclosure by the Government is
subject to restrictions as set forth in subparagraph
(c) of the Commercial Computer Software - Restricted
Rights clause at FAR sec. 52.227-19 and subparagraph
(c) (1) (ii) of the Rights in Technical Data and Computer
Software clause at DFARS sec. 252.227-7013.

           cisco Systems, Inc.
           170 West Tasman Drive
           San Jose, California 95134-1706



Cisco Internetwork Operating System Software
IOS (tm) PT1000 Software (PT1000-I-M), Version 12.2(28), RELEASE SOFTWARE (fc5)
Technical Support: http://www.cisco.com/techsupport
Copyright (c) 1986-2005 by cisco Systems, Inc.
Compiled Wed 27-Apr-04 19:01 by miwang

PT 1001 (PTSC2005) processor (revision 0x200) with 60416K/5120K bytes of memory
.
Processor board ID PT0123 (0123)
PT2005 processor: part number 0, mask 01
Bridging software.
X.25 software, Version 3.0.0.
4 FastEthernet/IEEE 802.3 interface(s)
2 Low-speed serial(sync/async) network interface(s)
32K bytes of non-volatile configuration memory.
63488K bytes of ATA CompactFlash (Read/Write)


         --- System Configuration Dialog ---

Continue with configuration dialog? [yes/no]: n


Press RETURN to get started!



Router>en
Router#config t
Enter configuration commands, one per line.  End with CNTL/Z.
Router(config)#int f0/0
Router(config-if)#ip add 10.0.0.1 255.255.255.0
Router(config-if)#no shut

Router(config-if)#
%LINK-5-CHANGED: Interface FastEthernet0/0, changed state to up

%LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up

Router(config-if)#ex
Router(config)#int f1/0
Router(config-if)#ip add 11.0.0.1 255.255.255.0
Router(config-if)#no shut

Router(config-if)#
%LINK-5-CHANGED: Interface FastEthernet1/0, changed state to up

%LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet1/0, changed state to up

Router(config-if)#
Router(config-if)#ex
Router(config)#ip dhcp pool Net1
Router(dhcp-config)#
Router(dhcp-config)#
Router(dhcp-config)#network 10.0.0.0 255.255.255.0
Router(dhcp-config)#
Router(dhcp-config)#def
% Incomplete command.
Router(dhcp-config)#def
Router(dhcp-config)#default-router 10.0.0.1
Router(dhcp-config)#
Router(dhcp-config)#ex
Router(config)#ip dhcp pool Net2
Router(dhcp-config)#network 11.0.0.0 255.255.255.0 
Router(dhcp-config)#def 11.0.0.1
Router(dhcp-config)#ex
Router(config)#
Router(config)#
Router(config)#%DHCPD-4-PING_CONFLICT: DHCP address conflict:  server pinged 10.0.0.1.
%DHCPD-4-PING_CONFLICT: DHCP address conflict:  server pinged 11.0.0.1.

Router(config)#
Router(config)#ip dhcp ex
Router(config)#ip dhcp excluded-address 10.0.0.2 10.0.0.10
Router(config)#ip dhcp ex
Router(config)#ip dhcp excluded-address 11.0.0.2 11.0.0.15
Router(config)#%DHCPD-4-PING_CONFLICT: DHCP address conflict:  server pinged 10.0.0.1.
%DHCPD-4-PING_CONFLICT: DHCP address conflict:  server pinged 11.0.0.1.

Router(config)#
Router(config)#no ip dhcp pool Net1
Router(config)#no ip dhcp pool Net2
Router(config)#








Router con0 is now available






Press RETURN to get started.













Router>
Router>en
Router#config t
Enter configuration commands, one per line.  End with CNTL/Z.
Router(config)#int f0/0
Router(config-if)#ip hel
Router(config-if)#ip help
Router(config-if)#ip helper-address 11.0.0.11
Router(config-if)#








Router con0 is now available






Press RETURN to get started.











